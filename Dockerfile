ARG ALPINE_VERSION=3

FROM alpine:${ALPINE_VERSION}

ENV PLAYGROUND_TITLE="GraphQL Playground" \
    PLAYGROUND_HOST="" \
    PLAYGROUND_ENDPOINT="/graphql" \
    PLAYGROUND_VERSION="1.7.20"

RUN set -x; \
    wget https://github.com/hairyhenderson/gomplate/releases/download/v3.8.0/gomplate_linux-amd64 \
    && chmod +x gomplate_linux-amd64 \
    && mv gomplate_linux-amd64 /usr/sbin/gomplate

COPY assets/index.html /

CMD ["ash", "-c", "while true; do echo -e 'HTTP/1.1 200 OK\\r\\n' | cat - /index.html | gomplate | nc -l -p 80 1>/dev/null; done"]

