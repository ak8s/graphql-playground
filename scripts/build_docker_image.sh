#!/usr/bin/env bash

set -e

while test $# -gt 0; do
  case $1 in
  -n | --name)
    shift
    if test $# -gt 0; then
      export IMAGE_NAME=$1
    else
      echo "no name specified"
      exit 1
    fi
    shift
    ;;
  -v | --version)
    shift
    if test $# -gt 0; then
      export VERSION=$1
    else
      echo "no version specified"
      exit 1
    fi
    shift
    ;;
  -p | --push)
    shift
    if test $# -gt 0; then
      export PUSH_IMAGE=$1
    else
      echo "no push value specified"
      exit 1
    fi
    shift
    ;;
  -l | --latest)
    shift
    if test $# -gt 0; then
      export TAG_LATEST=$1
    else
      echo "no tag latest value specified"
      exit 1
    fi
    shift
    ;;
  esac
done

IMAGE_NAME=${IMAGE_NAME:-'registry.gitlab.com/ak8s/graphql-playground'}
VERSION=${VERSION:-$(date -u '+%Y%m%d%H%M%S')}
PUSH_IMAGE=${PUSH_IMAGE:-'false'}
TAG_LATEST=${TAG_LATEST:-'true'}

echo "[name]: ${IMAGE_NAME}"
if [[ "${TAG_LATEST}" == "true" ]]; then
  echo "[tags]: ${VERSION}, latest"
else
  echo "[tag]: ${VERSION}"
fi
echo "[push] ${PUSH_IMAGE}"

docker build -t "${IMAGE_NAME}:${VERSION}" .

if [[ "${TAG_LATEST}" == "true" ]]; then
  docker tag "${IMAGE_NAME}:${VERSION}" "${IMAGE_NAME}:latest"
fi

if [[ "${PUSH_IMAGE}" == "true" ]]; then
  docker push "${IMAGE_NAME}:${VERSION}"
  if [[ "${TAG_LATEST}" == "true" ]]; then
    docker push "${IMAGE_NAME}:latest"
  fi
fi
